package gr.auth.ee.computer_networks.networkTests;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.SwingWorker;

import gr.auth.ee.computer_networks.Main;
import gr.auth.ee.computer_networks.helpers.TCPBundle;
import gr.auth.ee.computer_networks.helpers.UDPBundle;

public class Vehicle extends SwingWorker<Void, Void> {
	private static final int MAX_RESPONSE_LENGTH = 12;
	private static final String[] OBD_UDP_REQUEST_CODES = new String[] { "01 1F", "01 0F", "01 11", "01 0C", "01 0D",
			"01 05" };
	private static final int ENGINE_RUN_TIME = 0;
	private static final int INTAKE_AIR_TEMPERATURE = 1;
	private static final int THROTTLE_POSITION = 2;
	private static final int ENGINE_RPM = 3;
	private static final int VEHICLE_SPEED = 4;
	private static final int COOLANT_TEMPERATURE = 5;

	private final TCPBundle TCPConnection;
	private final UDPBundle UDPConnection;
	private String requestCode;
	private final boolean useUDP;
	private final int duration;
	private final JLabel lblVehicleEngineRunTimeOutput;
	private final JLabel lblVehicleAirTempOutput;
	private final JLabel lblVehicleThrottlePositionOutput;
	private final JLabel lblVehicleEngineRPMOutput;
	private final JLabel lblVehicleSpeedOutput;
	private final JLabel lblVehicleCoolantTemperatureOutput;
	private final JLabel lblVehiclePacketsTotalTimeOutput;

	private boolean testSuccess = false;
	private String testStatusOutput = "undefined";
	private String[] outputData = new String[7];
	private ArrayList<Integer> engineRunTimeValues = new ArrayList<>(), intakeAirTemperatureValues = new ArrayList<>(),
			vehicleSpeedValues = new ArrayList<>(), coolantTemperatureValues = new ArrayList<>();
	private ArrayList<Float> throttlePositionValues = new ArrayList<>(), engineRPMValues = new ArrayList<>();

	@SuppressWarnings("unused")
	private Vehicle() {
		// Disable default constructor
		this.TCPConnection = null;
		this.UDPConnection = null;
		this.useUDP = false;
		this.duration = 0;
		this.lblVehicleEngineRunTimeOutput = null;
		this.lblVehicleAirTempOutput = null;
		this.lblVehicleThrottlePositionOutput = null;
		this.lblVehicleEngineRPMOutput = null;
		this.lblVehicleSpeedOutput = null;
		this.lblVehicleCoolantTemperatureOutput = null;
		this.lblVehiclePacketsTotalTimeOutput = null;
	}

	public Vehicle(UDPBundle UDPConnection, String requestCode, int duration, JLabel lblVehicleEngineRunTimeOutput,
			JLabel lblVehicleAirTempOutput, JLabel lblVehicleThrottlePositionOutput, JLabel lblVehicleEngineRPMOutput,
			JLabel lblVehicleSpeedOutput, JLabel lblVehicleCoolantTemperatureOutput,
			JLabel lblVehiclePacketsTotalTimeOutput) {
		this.TCPConnection = null;
		this.UDPConnection = UDPConnection;
		this.requestCode = requestCode;
		this.useUDP = true;
		this.duration = duration;
		this.lblVehicleEngineRunTimeOutput = lblVehicleEngineRunTimeOutput;
		this.lblVehicleAirTempOutput = lblVehicleAirTempOutput;
		this.lblVehicleThrottlePositionOutput = lblVehicleThrottlePositionOutput;
		this.lblVehicleEngineRPMOutput = lblVehicleEngineRPMOutput;
		this.lblVehicleSpeedOutput = lblVehicleSpeedOutput;
		this.lblVehicleCoolantTemperatureOutput = lblVehicleCoolantTemperatureOutput;
		this.lblVehiclePacketsTotalTimeOutput = lblVehiclePacketsTotalTimeOutput;
	}

	public Vehicle(TCPBundle TCPConnection, String requestCode, int duration, JLabel lblVehicleEngineRunTimeOutput,
			JLabel lblVehicleAirTempOutput, JLabel lblVehicleThrottlePositionOutput, JLabel lblVehicleEngineRPMOutput,
			JLabel lblVehicleSpeedOutput, JLabel lblVehicleCoolantTemperatureOutput,
			JLabel lblVehiclePacketsTotalTimeOutput) {
		this.TCPConnection = TCPConnection;
		this.UDPConnection = null;
		this.requestCode = requestCode;
		this.useUDP = false;
		this.duration = duration;
		this.lblVehicleEngineRunTimeOutput = lblVehicleEngineRunTimeOutput;
		this.lblVehicleAirTempOutput = lblVehicleAirTempOutput;
		this.lblVehicleThrottlePositionOutput = lblVehicleThrottlePositionOutput;
		this.lblVehicleEngineRPMOutput = lblVehicleEngineRPMOutput;
		this.lblVehicleSpeedOutput = lblVehicleSpeedOutput;
		this.lblVehicleCoolantTemperatureOutput = lblVehicleCoolantTemperatureOutput;
		this.lblVehiclePacketsTotalTimeOutput = lblVehiclePacketsTotalTimeOutput;
	}

	private String getValueFromResponse(byte[] response, int bytesReturned, int loopIndex) {
		int returnValueXX = 0, returnValueYY = 0;
		String value = "";

		if (bytesReturned == 9) {
			String hexValueXX = "" + (char) response[6] + (char) response[7];

			returnValueXX = Integer.parseInt(hexValueXX, 16);
		} else if (bytesReturned == 12) {
			String hexValueXX = "" + (char) response[6] + (char) response[7];
			String hexValueYY = "" + (char) response[9] + (char) response[10];

			returnValueXX = Integer.parseInt(hexValueXX, 16);
			returnValueYY = Integer.parseInt(hexValueYY, 16);
		} else {
			// Shouldn't happen
			return null;
		}

		switch (loopIndex) {
		case ENGINE_RUN_TIME:
			value = "" + 256 * returnValueXX + returnValueYY;
			engineRunTimeValues.add(256 * returnValueXX + returnValueYY);
			break;
		case INTAKE_AIR_TEMPERATURE:
			value = "" + (returnValueXX - 40);
			intakeAirTemperatureValues.add(returnValueXX - 40);
			break;
		case THROTTLE_POSITION:
			value = "" + (100 * returnValueXX / 255);
			throttlePositionValues.add((float) (100 * returnValueXX / 255));
			break;
		case ENGINE_RPM:
			value = "" + ((256 * returnValueXX + returnValueYY) / 4);
			engineRPMValues.add((float) ((256 * returnValueXX + returnValueYY) / 4));
			break;
		case VEHICLE_SPEED:
			value = "" + returnValueXX;
			vehicleSpeedValues.add(returnValueXX);
			break;
		case COOLANT_TEMPERATURE:
			value = "" + (returnValueXX - 40);
			coolantTemperatureValues.add(returnValueXX - 40);
			break;
		default:
			break;
		}
		return value;
	}

	@Override
	protected Void doInBackground() {
		int vehicleOutputFileCounter = 0;
		byte responseBuffer[] = new byte[MAX_RESPONSE_LENGTH];
		long totalTimeElapsed = 0, OBDPacketStart = 0;
		FileOutputStream vehicleOutputStream = null;
		File vehicleOutputFile = null;
		do {
			vehicleOutputFile = new File("output/vehicle_" + (useUDP ? requestCode : "TCP")
					+ (vehicleOutputFileCounter == 0 ? "" : "_" + vehicleOutputFileCounter) + ".csv");
			if (!vehicleOutputFile.isFile()) {
				break;
			}
			++vehicleOutputFileCounter;
		} while (true);

		try {
			vehicleOutputStream = new FileOutputStream(vehicleOutputFile, false);
		} catch (FileNotFoundException exception) {
			exception.printStackTrace();
			testSuccess = false;
			testStatusOutput = "Vehicle file open failure!";
			return null;
		}

		if (useUDP) {
			DatagramPacket OBDRequestPacket = null,
					OBDResponsePacket = new DatagramPacket(responseBuffer, MAX_RESPONSE_LENGTH);

			while (totalTimeElapsed < duration * 1000) {
				for (int i = 0; i < OBD_UDP_REQUEST_CODES.length; ++i) {
					int bytesReturned = MAX_RESPONSE_LENGTH;
					String OBDRequestCode = requestCode + "OBD=" + OBD_UDP_REQUEST_CODES[i];
					OBDRequestPacket = new DatagramPacket(OBDRequestCode.getBytes(), OBDRequestCode.getBytes().length,
							UDPBundle.getHostAddress(), UDPConnection.getServerPort());

					try {
						UDPConnection.getConnection().send(OBDRequestPacket);
					} catch (IOException exception) {
						exception.printStackTrace();
						try {
							vehicleOutputStream.close();
						} catch (IOException inception) {
							inception.printStackTrace();
							testSuccess = false;
							testStatusOutput = "Vehicle file close failure, after request send failure!";
							return null;
						}
						testSuccess = false;
						testStatusOutput = "Request send failure!";
						return null;
					}

					OBDPacketStart = System.currentTimeMillis();
					try {
						UDPConnection.getConnection().receive(OBDResponsePacket);
					} catch (IOException exception) {
						exception.printStackTrace();
						try {
							vehicleOutputStream.close();
						} catch (IOException inception) {
							inception.printStackTrace();
							testSuccess = false;
							testStatusOutput = "Vehicle file close failure, after response receive failure!";
							return null;
						}
						testSuccess = false;
						testStatusOutput = "Response receive failure!";
						return null;
					}
					totalTimeElapsed += System.currentTimeMillis() - OBDPacketStart;

					for (int j = MAX_RESPONSE_LENGTH - 1; j > 0; --j) {
						if (responseBuffer[j] == 0) {
							--bytesReturned;
						}
					}

					outputData[i] = getValueFromResponse(responseBuffer, bytesReturned + 1, i);

					for (int j = 0; j < MAX_RESPONSE_LENGTH; ++j) {
						responseBuffer[j] = 0;
					}
				}

				outputData[6] = "" + totalTimeElapsed;
				publish();
			}
		} else {
			while (totalTimeElapsed < duration * 1000) {
				for (int i = 0; i < OBD_UDP_REQUEST_CODES.length; ++i) {
					int bytesReturned = 0;

					try {
						TCPConnection.getOutputStream().write((OBD_UDP_REQUEST_CODES[i] + (char) 13).getBytes());
					} catch (IOException exception) {
						exception.printStackTrace();
						try {
							vehicleOutputStream.close();
						} catch (IOException inception) {
							inception.printStackTrace();
							testSuccess = false;
							testStatusOutput = "Vehicle file close failure, after request send failure!";
							return null;
						}
						testSuccess = false;
						testStatusOutput = "Request send failure!";
						return null;
					}
					OBDPacketStart = System.currentTimeMillis();
					try {
						bytesReturned = TCPConnection.getInputStream().read(responseBuffer);
					} catch (IOException exception) {
						exception.printStackTrace();
						try {
							vehicleOutputStream.close();
						} catch (IOException inception) {
							inception.printStackTrace();
							testSuccess = false;
							testStatusOutput = "Vehicle file close failure, after response receive failure!";
							return null;
						}
						testSuccess = false;
						testStatusOutput = "Response receive failure!";
						return null;
					}
					totalTimeElapsed += System.currentTimeMillis() - OBDPacketStart;

					if (bytesReturned == -1) {
						try {
							vehicleOutputStream.close();
						} catch (IOException inception) {
							inception.printStackTrace();
							testSuccess = false;
							testStatusOutput = "Vehicle file close failure, after connection failure!";
							return null;
						}
						testSuccess = false;
						testStatusOutput = "Server closed the connection, try running the test again after restarting the app.";
						return null;
					} else if (bytesReturned != 9 && bytesReturned != 12) {
						try {
							vehicleOutputStream.close();
						} catch (IOException inception) {
							inception.printStackTrace();
							testSuccess = false;
							testStatusOutput = "Vehicle file close failure, after malformed response failure!";
							return null;
						}
						testSuccess = false;
						testStatusOutput = "Malformed response.";
						return null;
					}

					outputData[i] = getValueFromResponse(responseBuffer, bytesReturned, i);
				}
				outputData[6] = "" + totalTimeElapsed;
				publish();
			}
		}
		try {

			for (Integer engineRunTime : engineRunTimeValues) {
				vehicleOutputStream.write((engineRunTime + "\t").getBytes(), 0,
						(engineRunTime + "\t").getBytes().length);
			}
			vehicleOutputStream.write(("\n").getBytes(), 0, ("\n").getBytes().length);

			for (Integer intakeAirTemperatureValues : intakeAirTemperatureValues) {
				vehicleOutputStream.write((intakeAirTemperatureValues + "\t").getBytes(), 0,
						(intakeAirTemperatureValues + "\t").getBytes().length);
			}
			vehicleOutputStream.write(("\n").getBytes(), 0, ("\n").getBytes().length);

			for (Float throttlePositionValues : throttlePositionValues) {
				vehicleOutputStream.write((throttlePositionValues + "\t").getBytes(), 0,
						(throttlePositionValues + "\t").getBytes().length);
			}
			vehicleOutputStream.write(("\n").getBytes(), 0, ("\n").getBytes().length);

			for (Float engineRPMValues : engineRPMValues) {
				vehicleOutputStream.write((engineRPMValues + "\t").getBytes(), 0,
						(engineRPMValues + "\t").getBytes().length);
			}
			vehicleOutputStream.write(("\n").getBytes(), 0, ("\n").getBytes().length);

			for (Integer vehicleSpeedValues : vehicleSpeedValues) {
				vehicleOutputStream.write((vehicleSpeedValues + "\t").getBytes(), 0,
						(vehicleSpeedValues + "\t").getBytes().length);
			}
			vehicleOutputStream.write(("\n").getBytes(), 0, ("\n").getBytes().length);

			for (Integer coolantTemperatureValues : coolantTemperatureValues) {
				vehicleOutputStream.write((coolantTemperatureValues + "\t").getBytes(), 0,
						(coolantTemperatureValues + "\t").getBytes().length);
			}
		} catch (IOException exception) {
			exception.printStackTrace();
			try {
				vehicleOutputStream.close();
			} catch (IOException inception) {
				inception.printStackTrace();
				testSuccess = false;
				testStatusOutput = "Vehicle file close failure, after buffer write failure!";
				return null;
			}
			testSuccess = false;
			testStatusOutput = "Vehicle file buffer write failure!";
			return null;
		}

		try {
			vehicleOutputStream.flush();
			vehicleOutputStream.close();
		} catch (IOException exception) {
			exception.printStackTrace();
			testSuccess = false;
			testStatusOutput = "Vehicle file buffer flush/close failure!";
			return null;
		}
		testSuccess = true;
		testStatusOutput = "Test finished successfully.";
		return null;
	}

	@Override
	protected void process(List<Void> notInUse) {
		lblVehicleEngineRunTimeOutput.setText(outputData[0] + " s");
		lblVehicleAirTempOutput.setText(outputData[1] + " °C");
		lblVehicleThrottlePositionOutput.setText(outputData[2] + " %");
		lblVehicleEngineRPMOutput.setText(outputData[3] + " RPM");
		lblVehicleSpeedOutput.setText(outputData[4] + " Km/h");
		lblVehicleCoolantTemperatureOutput.setText(outputData[5] + " °C");
		lblVehiclePacketsTotalTimeOutput.setText(outputData[6] + " ms");
	}

	@Override
	protected void done() {
		Main.setStatusLineText(testStatusOutput,
				((testSuccess) ? Main.STATUS_LINE_ACTION_DONE : Main.STATUS_LINE_ACTION_ERROR));
		Main.setSubmitButtonsEnabled(true);
	}
}
