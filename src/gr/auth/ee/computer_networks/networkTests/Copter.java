package gr.auth.ee.computer_networks.networkTests;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import gr.auth.ee.computer_networks.Main;
import gr.auth.ee.computer_networks.helpers.MiniPID;
import gr.auth.ee.computer_networks.helpers.TCPBundle;

public class Copter extends SwingWorker<Void, Void> {
	private final static int PREAMPLE_RESPONSE_LENGTH = 429;
	private final static String PREAMPLE_RESPONSE_STRING = "HTTP/1.1 200 OK\r\n"
			+ "Server: Experimental Ithakicopter Java Server Version 0.4\r\n" + "Content-Type: text/html\r\n" + "\n"
			+ "\n" + "\n" + "Ithakicopter remote control.<br>\r\n"
			+ "Ready to enter a request-response control session.<br>\r\n" + "Request packet format :<br>\r\n"
			+ "AUTO FLIGHTLEVEL=FFF LMOTOR=LLL RMOTOR=RRR PILOT <CR><LF><br>\r\n" + "Response packet format :<br>\r\n"
			+ "ITHAKICOPTER LMOTOR=LLL RMOTOR=RRR ALTITUDE=AAA TEMPERATURE=TT.TT PRESSURE=PPPP.PP TELEMETRY <CR><LF><br>\r\n"
			+ "<br>\r\n";
	private final static int MAX_RESPONSE_LENGTH = 128;
	public final static String IMAGE_OUTPUT_URL = "http://ithaki.eng.auth.gr:38098/ithakicopter.msp&m=62&x=0&z=0&d=396&v=quad";
	public final static int IMAGE_OUTPUT_CROP_START_X = 4;
	public final static int IMAGE_OUTPUT_CROP_START_Y = 2;
	public final static int IMAGE_OUTPUT_CROP_WIDTH = 218;
	public final static int IMAGE_OUTPUT_CROP_HEIGHT = 486;

	private final TCPBundle TCPConnection;
	private final int desiredAltitude;
	private final JLabel lblCopterTemperatureOutput;
	private final JLabel lblCopterPressureOutput;
	private final JLabel lblCopterAltitudeOutput;
	private final JLabel lblCopterLeftMotorOutput;
	private final JLabel lblCopterRightMotorOutput;
	private final JLabel lblCopterImageOutput;
	private final JProgressBar progressBarCopterLeftMotorPower;
	private final JProgressBar progressBarCopterRightMotorPower;

	private boolean testSuccess = false;
	private String testStatusOutput = "undefined";

	private float copterOutputTemperature = 0, copterOutputPressure = 0;
	private int copterOutputAltitude = 54;
	private double copterOutputLeftMotor = 0, copterOutputRightMotor = 0;
	private BufferedImage copterImageOutput = null;

	@SuppressWarnings("unused")
	private Copter() {
		// Disable default constructor
		this.TCPConnection = null;
		this.desiredAltitude = 0;
		this.lblCopterTemperatureOutput = null;
		this.lblCopterPressureOutput = null;
		this.lblCopterAltitudeOutput = null;
		this.lblCopterLeftMotorOutput = null;
		this.lblCopterRightMotorOutput = null;
		this.lblCopterImageOutput = null;
		this.progressBarCopterLeftMotorPower = null;
		this.progressBarCopterRightMotorPower = null;
	}

	public Copter(TCPBundle TCPConnection, int desiredAltitude, JLabel lblCopterTemperatureOutput,
			JLabel lblCopterPressureOutput, JLabel lblCopterAltitudeOutput, JLabel lblCopterLeftMotorOutput,
			JLabel lblCopterRightMotorOutput, JLabel lblCopterImageOutput, JProgressBar progressBarCopterLeftMotorPower,
			JProgressBar progressBarCopterRightMotorPower) {
		this.TCPConnection = TCPConnection;
		this.desiredAltitude = desiredAltitude;
		this.lblCopterTemperatureOutput = lblCopterTemperatureOutput;
		this.lblCopterPressureOutput = lblCopterPressureOutput;
		this.lblCopterAltitudeOutput = lblCopterAltitudeOutput;
		this.lblCopterLeftMotorOutput = lblCopterLeftMotorOutput;
		this.lblCopterRightMotorOutput = lblCopterRightMotorOutput;
		this.lblCopterImageOutput = lblCopterImageOutput;
		this.progressBarCopterLeftMotorPower = progressBarCopterLeftMotorPower;
		this.progressBarCopterRightMotorPower = progressBarCopterRightMotorPower;
	}

	@Override
	protected Void doInBackground() {
		byte[] responseBuffer = new byte[PREAMPLE_RESPONSE_LENGTH];
		String requestCode = "GET /index.html HTTP/1.0\r\n\r\n", responseString = "";
		URL copterImageOutputUrl;

		try {
			copterImageOutputUrl = new URL(IMAGE_OUTPUT_URL);
		} catch (MalformedURLException exception) {
			// This should never happen
			exception.printStackTrace();
			testSuccess = false;
			testStatusOutput = "Malformed image URL error, this should never happen!";
			return null;
		}

		MiniPID miniPID = new MiniPID(0.058, 0.012, 0.009);
		miniPID.setOutputLimits(0, 105);
		miniPID.setSetpoint(desiredAltitude);

		try {
			TCPConnection.getOutputStream().write(requestCode.getBytes());
			TCPConnection.getInputStream().read(responseBuffer);
		} catch (IOException exception) {
			exception.printStackTrace();
			testSuccess = false;
			testStatusOutput = "Request send/response receive failure!";
			return null;
		}

		for (byte responseByte : responseBuffer) {
			responseString += (char) responseByte;
		}

		if (!responseString.isEmpty() && responseString.equals(PREAMPLE_RESPONSE_STRING)) {
			responseBuffer = new byte[MAX_RESPONSE_LENGTH];

			while (true) {
				int numberOfBytesRead, motorsLevel;

				responseString = "";
				motorsLevel = 150 + (int) Math.floor(miniPID.getOutput(copterOutputAltitude));
				requestCode = "AUTO FLIGHTLEVEL=" + desiredAltitude + " LMOTOR=" + motorsLevel + " RMOTOR="
						+ motorsLevel + " PILOT \r\n";

				try {
					TCPConnection.getOutputStream().write(requestCode.getBytes());
					numberOfBytesRead = TCPConnection.getInputStream().read(responseBuffer);

					if (numberOfBytesRead == -1) {
						testSuccess = false;
						testStatusOutput = "Server closed the connection, try running the test again after restarting the app.";
						return null;
					}
				} catch (SocketTimeoutException exception) {
					testSuccess = false;
					testStatusOutput = "Response timed out!";
					return null;
				} catch (IOException exception) {
					exception.printStackTrace();
					testSuccess = false;
					testStatusOutput = "Request send/response receive failure!";
					return null;
				}

				for (byte responseByte : responseBuffer) {
					responseString += (char) responseByte;
				}

				copterOutputLeftMotor = Integer.parseInt(responseString.substring(20, 23)) / 2.55;
				copterOutputRightMotor = Integer.parseInt(responseString.substring(31, 34)) / 2.55;
				copterOutputAltitude = Integer.parseInt(responseString.substring(44, 47));
				copterOutputTemperature = Float.parseFloat(responseString.substring(61, 66));
				copterOutputPressure = Float.parseFloat(responseString.substring(76, 83));
				try {
					copterImageOutput = ImageIO.read(copterImageOutputUrl);
				} catch (IOException exception) {
					exception.printStackTrace();
					testSuccess = false;
					testStatusOutput = "Image stream failure!";
					return null;
				}
				copterImageOutput = copterImageOutput.getSubimage(IMAGE_OUTPUT_CROP_START_X, IMAGE_OUTPUT_CROP_START_Y,
						IMAGE_OUTPUT_CROP_WIDTH, IMAGE_OUTPUT_CROP_HEIGHT);
				publish();
			}
		}
		testSuccess = true;
		testStatusOutput = "Test finished successfully.";
		return null;
	}

	@Override
	protected void process(List<Void> notInUse) {
		lblCopterTemperatureOutput.setText(copterOutputTemperature + " °C");
		lblCopterPressureOutput.setText(copterOutputPressure + " mBar");
		lblCopterAltitudeOutput.setText(copterOutputAltitude + " px above GND");
		lblCopterLeftMotorOutput.setText((new DecimalFormat("#.###")).format(copterOutputLeftMotor) + " %");
		lblCopterRightMotorOutput.setText((new DecimalFormat("#.###")).format(copterOutputRightMotor) + " %");

		progressBarCopterLeftMotorPower.setValue((int) Math.floor(copterOutputLeftMotor));
		progressBarCopterRightMotorPower.setValue((int) Math.floor(copterOutputRightMotor));

		lblCopterImageOutput.setIcon(new ImageIcon(copterImageOutput));
	}

	@Override
	protected void done() {
		Main.setStatusLineText(testStatusOutput,
				((testSuccess) ? Main.STATUS_LINE_ACTION_DONE : Main.STATUS_LINE_ACTION_ERROR));
		Main.setSubmitButtonsEnabled(true);
	}
}
