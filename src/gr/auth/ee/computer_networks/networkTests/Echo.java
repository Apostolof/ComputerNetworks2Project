package gr.auth.ee.computer_networks.networkTests;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.util.List;

import javax.swing.JTextPane;
import javax.swing.SwingWorker;

import gr.auth.ee.computer_networks.Main;
import gr.auth.ee.computer_networks.helpers.UDPBundle;

public class Echo extends SwingWorker<Void, Long> {
	private static final int MAX_RESPONSE_LENGTH = 2048;

	private static UDPBundle UDPConnection;
	private String requestCode;
	private int duration, numberOfPackages;
	private boolean getTemperature;
	private static JTextPane runtimeOutput;

	private boolean testSuccess = false;
	private String testStatusOutput = "undefined";

	@SuppressWarnings("unused")
	private Echo() {
		// Disable default constructor
	}

	public Echo(UDPBundle UDPConnection, String requestCode, int duration, int numberOfPackages, boolean getTemperature,
			JTextPane runtimeOutput) {
		Echo.UDPConnection = UDPConnection;
		this.requestCode = requestCode;
		this.duration = duration;
		this.numberOfPackages = numberOfPackages;
		this.getTemperature = getTemperature;
		Echo.runtimeOutput = runtimeOutput;
	}

	@Override
	protected Void doInBackground() {
		int currentPackageNumber = 0, echoOutputFileCounter = 0;
		boolean monitorNumberOfPackages = !(numberOfPackages == 0), monitorDuration = !(duration == 0);
		byte responseBuffer[] = new byte[MAX_RESPONSE_LENGTH];
		DatagramPacket echoRequestPacket = null,
				echoResponsePacket = new DatagramPacket(responseBuffer, MAX_RESPONSE_LENGTH);
		long startTime = System.currentTimeMillis(), packageStartTime = 0, packageEndTime, runningTime = 0;
		String temperatureString = null;
		FileOutputStream echoOutputStream = null;
		File echoOutputFile = null;
		do {
			echoOutputFile = new File("output/echo_" + requestCode
					+ (echoOutputFileCounter == 0 ? "" : "_" + echoOutputFileCounter) + ".csv");
			if (!echoOutputFile.isFile()) {
				break;
			}
			++echoOutputFileCounter;
		} while (true);

		try {
			echoOutputStream = new FileOutputStream(echoOutputFile, false);
		} catch (FileNotFoundException exception) {
			exception.printStackTrace();
			testSuccess = false;
			testStatusOutput = "Echo file open failure!";
			return null;
		}

		if (getTemperature) {
			requestCode = requestCode + "T00";
		}

		requestCode = requestCode + "\r";

		echoRequestPacket = new DatagramPacket(requestCode.getBytes(), requestCode.getBytes().length,
				UDPBundle.getHostAddress(), UDPConnection.getServerPort());

		while (true) {
			currentPackageNumber++;
			try {
				UDPConnection.getConnection().send(echoRequestPacket);
			} catch (IOException exception) {
				exception.printStackTrace();
				try {
					echoOutputStream.close();
				} catch (IOException inception) {
					inception.printStackTrace();
					testSuccess = false;
					testStatusOutput = "Echo file close failure, after request send failure!";
					return null;
				}
				testSuccess = false;
				testStatusOutput = "Request send failure!";
				return null;
			}

			try {
				packageStartTime = System.currentTimeMillis();
				UDPConnection.getConnection().receive(echoResponsePacket);
			} catch (IOException exception) {
				exception.printStackTrace();
				try {
					echoOutputStream.close();
				} catch (IOException inception) {
					inception.printStackTrace();
					testSuccess = false;
					testStatusOutput = "Echo file close failure, after response receive failure!";
					return null;
				}
				testSuccess = false;
				testStatusOutput = "Response receive failure!";
				return null;
			}

			String responseString = new String(responseBuffer);
			if (!responseString.contains("PSTART") || !responseString.contains("PSTOP")) {
				try {
					echoOutputStream.close();
				} catch (IOException inception) {
					inception.printStackTrace();
					testSuccess = false;
					testStatusOutput = "Echo file close failure, after malformed response receive!";
					return null;
				}
				testSuccess = false;
				testStatusOutput = "Malformed response!";
				return null;
			} else {
				packageEndTime = System.currentTimeMillis();
				long packageTime = packageEndTime - packageStartTime;
				runningTime += packageTime;

				try {
					echoOutputStream.write((packageTime + "\t").getBytes(), 0, (packageTime + "\t").getBytes().length);
				} catch (IOException exception) {
					exception.printStackTrace();
					try {
						echoOutputStream.close();
					} catch (IOException inception) {
						inception.printStackTrace();
						testSuccess = false;
						testStatusOutput = "Echo file close failure, after buffer write failure!";
						return null;
					}
					testSuccess = false;
					testStatusOutput = "Echo file buffer write failure!";
					return null;
				}

				if (getTemperature) {
					if (currentPackageNumber == 1) {
						if (responseString.contains("+")) {
							temperatureString = responseString.substring(responseString.indexOf("+"),
									responseString.indexOf(" C"));
						} else {
							temperatureString = responseString.substring(responseString.indexOf("-"),
									responseString.indexOf(" C"));
						}
					}
					publish(packageTime, runningTime / currentPackageNumber, Long.parseLong(temperatureString));
				} else {
					publish(packageTime, runningTime / currentPackageNumber, null);
				}
			}

			if (monitorNumberOfPackages) {
				if (numberOfPackages == currentPackageNumber) {
					try {
						echoOutputStream.flush();
						echoOutputStream.close();
					} catch (IOException exception) {
						exception.printStackTrace();
						testSuccess = false;
						testStatusOutput = "Echo file buffer flush/close failure!";
						return null;
					}
					testSuccess = true;
					testStatusOutput = "Test finished successfully.";
					return null;
				}
			}
			if (monitorDuration) {
				if ((packageEndTime - startTime) >= (duration * 1000)) {
					try {
						echoOutputStream.flush();
						echoOutputStream.close();
					} catch (IOException exception) {
						exception.printStackTrace();
						testSuccess = false;
						testStatusOutput = "Echo file buffer flush/close failure!";
						return null;
					}
					testSuccess = true;
					testStatusOutput = "Test finished successfully.";
					return null;
				}
			}
		}
	}

	@Override
	protected void process(List<Long> pingValues) {
		if (pingValues.size() != 3) {
			return;
		} else {
			if (pingValues.get(2) == null) {
				runtimeOutput.setText("Last packet ping time:\t" + pingValues.get(0)
						+ " ms\nAverage packet ping time:\t" + pingValues.get(1) + " ms\nTemperature = NaN");
			} else {
				runtimeOutput
						.setText("Last packet ping time:\t" + pingValues.get(0) + " ms\nAverage packet ping time:\t"
								+ pingValues.get(1) + " ms\nTemperature = " + pingValues.get(2) + " °C");
			}
		}
	}

	@Override
	protected void done() {
		Main.setStatusLineText(testStatusOutput,
				((testSuccess) ? Main.STATUS_LINE_ACTION_DONE : Main.STATUS_LINE_ACTION_ERROR));
		Main.setSubmitButtonsEnabled(true);
	}
}
