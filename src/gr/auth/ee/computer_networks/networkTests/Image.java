package gr.auth.ee.computer_networks.networkTests;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.SwingWorker;

import gr.auth.ee.computer_networks.Main;
import gr.auth.ee.computer_networks.helpers.UDPBundle;

public class Image extends SwingWorker<Void, Boolean> {
	private static final int IMAGE_WIDTH = 320;
	private static final int IMAGE_HEIGHT = 240;

	private static UDPBundle UDPConnection;
	private String requestCode;
	private int duration, packageLength;
	private boolean flowControl;
	private static JTextPane runtimeStatsOutput;
	private static JLabel runtimeImageOutput;
	private ArrayList<Byte> dispImageBytesList;

	private boolean testSuccess = false;
	private String testStatusOutput = "undefined";
	private long currentImageTimeElapsed = 0;
	private int currentImageNumberOfPackages = 0;
	private float averageImageTimeElapsed = 0, averageImageNumberOfPackages = 0, currentImageSize = 0,
			averageImageSize = 0, fps = 0;

	@SuppressWarnings("unused")
	private Image() {
		// Disable default constructor
	}

	public Image(UDPBundle UDPConnection, String requestCode, int duration, boolean flowControl, int packageLength,
			JTextPane runtimeStatsOutput, JLabel runtimeImageOutput) {
		Image.UDPConnection = UDPConnection;
		this.requestCode = requestCode;
		this.duration = duration;
		this.packageLength = packageLength;
		this.flowControl = flowControl;
		Image.runtimeStatsOutput = runtimeStatsOutput;
		Image.runtimeImageOutput = runtimeImageOutput;
	}

	@Override
	protected Void doInBackground() {
		boolean monitorDuration = !(duration == 0);
		byte responseBuffer[] = new byte[packageLength];
		DatagramPacket imageRequestPacket = null,
				imageResponsePacket = new DatagramPacket(responseBuffer, packageLength), imageNextPacket = null;
		long startTime = System.currentTimeMillis(), totalImagesTimeElapsed = 0;
		int numberOfImages = 0, totalImagesNumberOfPackages = 0, totalImagesSize = 0;

		averageImageTimeElapsed = 0;
		averageImageNumberOfPackages = 0;
		averageImageSize = 0;
		fps = 0;

		if (flowControl) {
			requestCode = requestCode + "FLOW=ON";
			imageNextPacket = new DatagramPacket("NEXT".getBytes(), "NEXT".getBytes().length,
					UDPBundle.getHostAddress(), UDPConnection.getServerPort());
		}
		if (packageLength != 128) {
			requestCode = requestCode + "UDP=" + packageLength;
		}

		requestCode = requestCode + "\r";

		imageRequestPacket = new DatagramPacket(requestCode.getBytes(), requestCode.getBytes().length,
				UDPBundle.getHostAddress(), UDPConnection.getServerPort());

		while (true) {
			ArrayList<Byte> loadingImageBytesList = new ArrayList<Byte>();
			FileOutputStream imageOutputStream = null;
			try {
				imageOutputStream = new FileOutputStream("output/image_" + requestCode + "_" + numberOfImages + ".jpg",
						false);
			} catch (FileNotFoundException exception) {
				exception.printStackTrace();
				testSuccess = false;
				testStatusOutput = "Image file open failure!";
				return null;
			}
			boolean shouldBreak = false;
			currentImageNumberOfPackages = 0;
			currentImageTimeElapsed = 0;
			currentImageSize = 0;
			try {
				UDPConnection.getConnection().send(imageRequestPacket);
			} catch (IOException exception) {
				exception.printStackTrace();
				try {
					imageOutputStream.close();
				} catch (IOException inception) {
					inception.printStackTrace();
					testSuccess = false;
					testStatusOutput = "Image file close failure, after request send failure!";
					return null;
				}
				testSuccess = false;
				testStatusOutput = "Request send failure!";
				return null;
			}
			long currentImageTimeStart = System.currentTimeMillis();

			while (true) {
				try {
					UDPConnection.getConnection().receive(imageResponsePacket);
				} catch (IOException exception) {
					exception.printStackTrace();
					try {
						imageOutputStream.close();
					} catch (IOException inception) {
						inception.printStackTrace();
						testSuccess = false;
						testStatusOutput = "Image file close failure, after response receive failure!";
						return null;
					}
					testSuccess = false;
					testStatusOutput = "Response receive failure!";
					return null;
				}

				++currentImageNumberOfPackages;

				try {
					imageOutputStream.write(responseBuffer, 0, responseBuffer.length);
				} catch (IOException exception) {
					exception.printStackTrace();
					try {
						imageOutputStream.close();
					} catch (IOException inception) {
						inception.printStackTrace();
						testSuccess = false;
						testStatusOutput = "Image file close failure, after buffer write failure!";
						return null;
					}
					testSuccess = false;
					testStatusOutput = "Image file buffer write failure!";
					return null;
				}

				for (int i = 0; i < packageLength; ++i) {
					loadingImageBytesList.add(responseBuffer[i]);

					if (responseBuffer[i] != 0) {
						if ((i < packageLength - 1) && (responseBuffer[i] == (byte) 255)
								&& (responseBuffer[i + 1] == (byte) 217)) {
							shouldBreak = true;
						}

						responseBuffer[i] = 0;
					}
				}

				if (currentImageNumberOfPackages % 20 == 0) {
					currentImageSize = (float) ((loadingImageBytesList.size()) / 1000.0);
					currentImageTimeElapsed = System.currentTimeMillis() - currentImageTimeStart;
					publish(false);
				}

				if (shouldBreak) {
					currentImageTimeElapsed = System.currentTimeMillis() - currentImageTimeStart;
					break;
				}

				if (flowControl) {
					try {
						UDPConnection.getConnection().send(imageNextPacket);
					} catch (IOException exception) {
						exception.printStackTrace();
						try {
							imageOutputStream.close();
						} catch (IOException inception) {
							inception.printStackTrace();
							testSuccess = false;
							testStatusOutput = "Image file close failure, after request send failure!";
							return null;
						}
						testSuccess = false;
						testStatusOutput = "Request send failure!";
						return null;
					}
				}
			}

			try {
				imageOutputStream.flush();
				imageOutputStream.close();
			} catch (IOException exception) {
				exception.printStackTrace();
				testSuccess = false;
				testStatusOutput = "Image file buffer flush/close failure!";
				return null;
			}

			++numberOfImages;
			totalImagesTimeElapsed += currentImageTimeElapsed;
			averageImageTimeElapsed = (float) totalImagesTimeElapsed / (float) numberOfImages;
			totalImagesNumberOfPackages += currentImageNumberOfPackages;
			averageImageNumberOfPackages = (float) totalImagesNumberOfPackages / (float) numberOfImages;
			totalImagesSize += currentImageSize;
			averageImageSize = totalImagesSize / (float) numberOfImages;
			dispImageBytesList = loadingImageBytesList;
			fps = (float) ((numberOfImages) / (totalImagesTimeElapsed / 1000.0));
			publish(true);

			if (monitorDuration) {
				if ((System.currentTimeMillis() - startTime) >= (duration * 1000)) {
					break;
				}
			}
		}
		testSuccess = true;
		testStatusOutput = "Test finished successfully.";
		return null;
	}

	@Override
	protected void process(List<Boolean> shouldRefreshImage) {
		runtimeStatsOutput.setText("Current Image" + "\t" + "\t" + "Average" + "\n" + "Time elapsed = "
				+ currentImageTimeElapsed + " ms\t" + "Average image time = " + averageImageTimeElapsed + " ms\n"
				+ "Number of packages = " + currentImageNumberOfPackages + "\t" + "Average number of packages = "
				+ averageImageNumberOfPackages + "\n" + "Image size = " + currentImageSize + " KB\t"
				+ "Average image size = " + averageImageSize + " KB\n" + "\n" + "FPS = " + fps);

		if (shouldRefreshImage.get(0)) {
			byte[] imageBytes = new byte[dispImageBytesList.size()];
			for (int i = 0; i < dispImageBytesList.size(); ++i) {
				imageBytes[i] = dispImageBytesList.get(i);
			}

			ImageIcon image = new ImageIcon(imageBytes);

			BufferedImage resizedImg = new BufferedImage(IMAGE_WIDTH, IMAGE_HEIGHT, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g2 = resizedImg.createGraphics();

			g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			g2.drawImage(image.getImage(), 0, 0, IMAGE_WIDTH, IMAGE_HEIGHT, null);
			g2.dispose();
			image = new ImageIcon(resizedImg);

			runtimeImageOutput.setIcon(image);
		}
	}

	@Override
	protected void done() {
		Main.setStatusLineText(testStatusOutput,
				((testSuccess) ? Main.STATUS_LINE_ACTION_DONE : Main.STATUS_LINE_ACTION_ERROR));
		Main.setSubmitButtonsEnabled(true);
	}
}
