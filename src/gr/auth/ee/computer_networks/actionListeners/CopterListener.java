package gr.auth.ee.computer_networks.actionListeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JSlider;

import gr.auth.ee.computer_networks.Main;
import gr.auth.ee.computer_networks.helpers.TCPBundle;
import gr.auth.ee.computer_networks.networkTests.Copter;

public class CopterListener implements ActionListener {
	private final JSlider sliderCopterFlightLevel;
	private final JLabel lblCopterTemperatureOutput;
	private final JLabel lblCopterPressureOutput;
	private final JLabel lblCopterAltitudeOutput;
	private final JLabel lblCopterLeftMotorOutput;
	private final JLabel lblCopterRightMotorOutput;
	private final JLabel lblCopterImageOutput;
	private final JProgressBar progressBarCopterLeftMotorPower;
	private final JProgressBar progressBarCopterRightMotorPower;
	private final TCPBundle TCPConnection;

	@SuppressWarnings("unused")
	private CopterListener() {
		// Disable default constructor
		this.sliderCopterFlightLevel = null;
		this.lblCopterTemperatureOutput = null;
		this.lblCopterPressureOutput = null;
		this.lblCopterAltitudeOutput = null;
		this.lblCopterLeftMotorOutput = null;
		this.lblCopterRightMotorOutput = null;
		this.lblCopterImageOutput = null;
		this.progressBarCopterLeftMotorPower = null;
		this.progressBarCopterRightMotorPower = null;
		this.TCPConnection = null;
	}

	public CopterListener(JSlider sliderCopterFlightLevel, JLabel lblCopterTemperatureOutput,
			JLabel lblCopterPressureOutput, JLabel lblCopterAltitudeOutput, JLabel lblCopterLeftMotorOutput,
			JLabel lblCopterRightMotorOutput, JLabel lblCopterImageOutput, JProgressBar progressBarCopterLeftMotorPower,
			JProgressBar progressBarCopterRightMotorPower, TCPBundle TCPConnection) {
		this.sliderCopterFlightLevel = sliderCopterFlightLevel;
		this.lblCopterTemperatureOutput = lblCopterTemperatureOutput;
		this.lblCopterPressureOutput = lblCopterPressureOutput;
		this.lblCopterAltitudeOutput = lblCopterAltitudeOutput;
		this.lblCopterLeftMotorOutput = lblCopterLeftMotorOutput;
		this.lblCopterRightMotorOutput = lblCopterRightMotorOutput;
		this.lblCopterImageOutput = lblCopterImageOutput;
		this.progressBarCopterLeftMotorPower = progressBarCopterLeftMotorPower;
		this.progressBarCopterRightMotorPower = progressBarCopterRightMotorPower;
		this.TCPConnection = TCPConnection;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Main.setSubmitButtonsEnabled(false);
		Main.setStatusLineText("Test running...", Main.STATUS_LINE_ACTION_RUNNING);
		(new Copter(TCPConnection, sliderCopterFlightLevel.getValue(), lblCopterTemperatureOutput,
				lblCopterPressureOutput, lblCopterAltitudeOutput, lblCopterLeftMotorOutput, lblCopterRightMotorOutput,
				lblCopterImageOutput, progressBarCopterLeftMotorPower, progressBarCopterRightMotorPower)).execute();
	}
}
