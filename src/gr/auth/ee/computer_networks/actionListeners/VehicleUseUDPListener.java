package gr.auth.ee.computer_networks.actionListeners;

import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class VehicleUseUDPListener implements ChangeListener {
	private final JFormattedTextField formatedTextFieldVehicleRequestCode;

	@SuppressWarnings("unused")
	private VehicleUseUDPListener() {
		// Disable default constructor
		this.formatedTextFieldVehicleRequestCode = null;
	}

	public VehicleUseUDPListener(JFormattedTextField formatedTextFieldVehicleRequestCode) {
		// Disable default constructor
		this.formatedTextFieldVehicleRequestCode = formatedTextFieldVehicleRequestCode;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		this.formatedTextFieldVehicleRequestCode.setEnabled(((JCheckBox) e.getSource()).isSelected());
	}
}
