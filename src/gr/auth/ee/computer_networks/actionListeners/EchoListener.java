package gr.auth.ee.computer_networks.actionListeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import gr.auth.ee.computer_networks.Main;
import gr.auth.ee.computer_networks.helpers.UDPBundle;
import gr.auth.ee.computer_networks.networkTests.Echo;

public class EchoListener implements ActionListener {

	private final JTextField textEchoRequestCode, textEchoDuration, textEchoNumberOfPackages;
	private final JCheckBox checkBoxGetTemperature;
	private final JTextPane runtimeOutput;
	private final UDPBundle UDPConnection;

	@SuppressWarnings("unused")
	private EchoListener() {
		// Disable default constructor
		this.textEchoRequestCode = null;
		this.textEchoDuration = null;
		this.textEchoNumberOfPackages = null;
		this.checkBoxGetTemperature = null;
		this.runtimeOutput = null;
		this.UDPConnection = null;
	}

	public EchoListener(JTextField textEchoRequestCode, JTextField textEchoDuration,
			JTextField textEchoNumberOfPackages, JCheckBox checkBoxGetTemperature, JTextPane runtimeOutput,
			UDPBundle UDPConnection) {
		this.textEchoRequestCode = textEchoRequestCode;
		this.textEchoDuration = textEchoDuration;
		this.textEchoNumberOfPackages = textEchoNumberOfPackages;
		this.checkBoxGetTemperature = checkBoxGetTemperature;
		this.runtimeOutput = runtimeOutput;
		this.UDPConnection = UDPConnection;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Main.setSubmitButtonsEnabled(false);
		Main.setStatusLineText("Test running...", Main.STATUS_LINE_ACTION_RUNNING);
		(new Echo(UDPConnection, textEchoRequestCode.getText(), Integer.parseInt(textEchoDuration.getText()),
				Integer.parseInt(textEchoNumberOfPackages.getText()), checkBoxGetTemperature.isSelected(),
				runtimeOutput)).execute();
	}
}