package gr.auth.ee.computer_networks.actionListeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFormattedTextField;

import gr.auth.ee.computer_networks.Main;
import gr.auth.ee.computer_networks.helpers.UDPBundle;

public class GlobalSettingsListener implements ActionListener {
	private final JFormattedTextField formatedTextFieldServerPort, formatedTextFieldClientPort;
	private final UDPBundle UDPConnection;

	@SuppressWarnings("unused")
	private GlobalSettingsListener() {
		// Disable default constructor
		this.formatedTextFieldServerPort = null;
		this.formatedTextFieldClientPort = null;
		UDPConnection = null;
	}

	public GlobalSettingsListener(JFormattedTextField formatedTextFieldServerPort,
			JFormattedTextField formatedTextFieldClientPort, UDPBundle UDPConnection) {
		this.formatedTextFieldServerPort = formatedTextFieldServerPort;
		this.formatedTextFieldClientPort = formatedTextFieldClientPort;
		this.UDPConnection = UDPConnection;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Main.setSubmitButtonsEnabled(false);
		int newLocalPort = Integer.parseInt(formatedTextFieldClientPort.getText());
		int newServerPort = Integer.parseInt(formatedTextFieldServerPort.getText());

		UDPConnection.setLocalPort(newLocalPort);
		UDPConnection.setServerPort(newServerPort);
		Main.setStatusLineText("Ports set.", Main.STATUS_LINE_ACTION_DONE);
		Main.setSubmitButtonsEnabled(true);
	}
}
