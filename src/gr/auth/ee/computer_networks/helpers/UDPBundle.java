package gr.auth.ee.computer_networks.helpers;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class UDPBundle {
	private static final int CONNECTION_TIME_OUT = 10000;

	private String serverIp = "";
	private int serverPort = 0, localPort = 0;
	private DatagramSocket connection = null;
	private static InetAddress hostAddress;

	@SuppressWarnings("unused")
	private UDPBundle() {
		// Disable default constructor
	}

	public UDPBundle(String serverIp, int serverPort, int localPort) {
		this.serverIp = serverIp;
		this.serverPort = serverPort;
		this.localPort = localPort;
		try {
			connection = new DatagramSocket(this.localPort);
			connection.setSoTimeout(CONNECTION_TIME_OUT);

			String[] serverIPSplit = serverIp.split("\\.");
			byte[] serverIPBytes = { (byte) Integer.parseInt(serverIPSplit[0]),
					(byte) Integer.parseInt(serverIPSplit[1]), (byte) Integer.parseInt(serverIPSplit[2]),
					(byte) Integer.parseInt(serverIPSplit[3]) };
			hostAddress = InetAddress.getByAddress(serverIPBytes);
		} catch (SocketException exception) {
			exception.printStackTrace();
		} catch (UnknownHostException exception) {
			exception.printStackTrace();
		}
	}

	public String getServerIp() {
		return serverIp;
	}

	public int getServerPort() {
		return serverPort;
	}

	public int getLocalPort() {
		return localPort;
	}

	public DatagramSocket getConnection() {
		return connection;
	}

	public static InetAddress getHostAddress() {
		return hostAddress;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	public boolean setLocalPort(int localPort) {
		if (this.localPort == localPort) {
			return true;
		}
		try {
			connection = new DatagramSocket(localPort);
			this.localPort = localPort;
			return true;
		} catch (SocketException exception) {
			exception.printStackTrace();
			return false;
		}
	}
}
